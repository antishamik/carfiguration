from .configure import *
from .report import *

class ExceptionResponseSchema(BaseModel):
    error: str
