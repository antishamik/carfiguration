from pydantic import BaseModel


class GetReportListResponseSchema(BaseModel):
    id: int
    username: str
    price: float