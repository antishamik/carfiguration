from pydantic import BaseModel


class GetEntityListResponseSchema(BaseModel):
    id: int
    name: str
    price: float


class GetCarListResponseSchema(GetEntityListResponseSchema):
    pass


class GetBatteryListResponseSchema(GetEntityListResponseSchema):
    pass


class GetWheelListResponseSchema(GetEntityListResponseSchema):
    active: bool


class GetTireListResponseSchema(GetEntityListResponseSchema):
    active: bool

class CreateReportRequestSchema(BaseModel):
    username: str
    price: float

class CreateReportResponseSchema(BaseModel):
    id: int
    username: str
    price: float

    class Config:
        orm_mode = True