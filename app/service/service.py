from typing import List, Union, NoReturn, Any

from app.models import Car, Battery, Wheel, Tire, Report
from core.db import session, Transaction, Propagation
from core.utils import WheelRule, TireRule, DISCOUNT_PRICE
from core.utils.time_functions import TimeFunctions


class ConfigurationInterface:
    def configure(self, entity, rule, condition) -> bool:
        if condition in rule.get(entity, []):
            return True
        return False


class CarService:
    def __init__(self):
        pass

    def check_last_friday(self):
        the_last_day = TimeFunctions.get_the_last_day(target_dayofweek=4)
        current_date = TimeFunctions.current_date()
        if the_last_day == current_date:
            return True
        return False

    def apply_discount(self, cars):
        for car in cars:
            if self.check_last_friday():
                car['price'] = car['price'] - DISCOUNT_PRICE


    async def get_cars_list(self) -> list[dict[str, Any]]:
        cars = [{'name': u.__dict__['name'],
                 'id': u.__dict__['id'],
                 'price': u.__dict__['price']} for u in session.query(Car).all()]
        self.apply_discount(cars)
        return cars


class BatteryService():
    def __init__(self):
        pass

    async def get_batteries_list(self) -> list[dict[str, Any]]:
        batteries = [{'name': u.__dict__['name'],
                      'id': u.__dict__['id'],
                      'price': u.__dict__['price']} for u in session.query(Battery).all()]
        return batteries


class TireService(ConfigurationInterface):
    def __init__(self):
        pass

    async def get_tires_list(self) -> list[dict[str, Any]]:
        tires = [{'name': u.__dict__['name'],
                  'id': u.__dict__['id'],
                  'price': u.__dict__['price']} for u in session.query(Tire).all()]
        return tires

    async def get_configured_tires(self, tires, condition) -> List:
        for tire in tires:
            tire['active'] = self.configure(tire.get('name', ''), TireRule, condition)
        return tires


class WheelService(ConfigurationInterface):
    def __init__(self):
        pass

    async def get_wheels_list(self) -> list[dict[str, Any]]:
        wheels = [{'name': u.__dict__['name'],
                   'id': u.__dict__['id'],
                   'price': u.__dict__['price']} for u in session.query(Wheel).all()]
        return wheels

    async def get_configured_wheels(self, wheels, condition) -> List:
        for wheel in wheels:
            wheel['active'] = self.configure(wheel.get('name', ''), WheelRule, condition)
        return wheels


class ReportService:
    def __init__(self):
        pass

    @Transaction(propagation=Propagation.REQUIRED)
    async def create_report(
            self, username: str, price: float
    ) -> Union[Report, NoReturn]:
        report = Report(username=username, price=price)
        session.add(report)

        return report

    async def get_report_list(self) -> list[dict[str, Any]]:
        reports = [{'username': u.__dict__['username'],
                 'id': u.__dict__['id'],
                 'price': u.__dict__['price']} for u in session.query(Report).all()]
        return reports
