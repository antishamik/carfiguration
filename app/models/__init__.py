from sqlalchemy import Column, Unicode, BigInteger

from core.db import Base
from core.db.mixins import TimestampMixin
from sqlalchemy import Numeric


class Car(Base, TimestampMixin):
    __tablename__ = "cars"
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    name = Column(Unicode(255), nullable=False)
    price = Column(Numeric(10, 2))


class Wheel(Base, TimestampMixin):
    __tablename__ = "wheels"
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    name = Column(Unicode(255), nullable=False)
    price = Column(Numeric(10, 2))


class Tire(Base, TimestampMixin):
    __tablename__ = "tires"
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    name = Column(Unicode(255), nullable=False)
    price = Column(Numeric(10, 2))


class Battery(Base, TimestampMixin):
    __tablename__ = "batteries"
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    name = Column(Unicode(255), nullable=False)
    price = Column(Numeric(10, 2))


class Report(Base, TimestampMixin):
    __tablename__ = "reports"
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    username = Column(Unicode(255), nullable=False)
    price = Column(Numeric(10, 2))
