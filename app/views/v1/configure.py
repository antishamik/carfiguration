from typing import List

from fastapi import APIRouter

from app.schemas import (
    ExceptionResponseSchema,
    GetCarListResponseSchema,
    GetBatteryListResponseSchema,
    GetTireListResponseSchema,
    GetWheelListResponseSchema, CreateReportResponseSchema, CreateReportRequestSchema
)
from app.service import CarService, BatteryService, WheelService, TireService, ReportService

configure_router = APIRouter()

@configure_router.get(
    "/car",
    response_model=List[GetCarListResponseSchema],
    responses={"400": {"model": ExceptionResponseSchema}},
)
async def get_car_list():
    return await CarService().get_cars_list()

@configure_router.get(
    "/battery",
    response_model=List[GetBatteryListResponseSchema],
    responses={"400": {"model": ExceptionResponseSchema}},
)
async def get_batteries_list():
    return await BatteryService().get_batteries_list()


@configure_router.get(
    "/wheel/{battery_name}",
    response_model=List[GetWheelListResponseSchema],
    responses={"400": {"model": ExceptionResponseSchema}},
)
async def get_wheels_list(battery_name):
    service = WheelService()
    wheels = await service.get_wheels_list()
    return await service.get_configured_wheels(wheels, condition=battery_name)


@configure_router.get(
    "/tire/{wheel_name}",
    response_model=List[GetTireListResponseSchema],
    responses={"400": {"model": ExceptionResponseSchema}},
)
async def get_tires_list(wheel_name):
    service = TireService()
    wheels = await service.get_tires_list()
    return await service.get_configured_tires(wheels, condition=wheel_name)

@configure_router.post(
    "/save",
    response_model=CreateReportResponseSchema,
    responses={"400": {"model": ExceptionResponseSchema}},
)
async def create_report(request: CreateReportRequestSchema):
    return await ReportService().create_report(**request.dict())