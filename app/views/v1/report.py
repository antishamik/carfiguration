from typing import List

from app.schemas import (
    ExceptionResponseSchema,
    GetReportListResponseSchema,
)

from app.service import ReportService

from fastapi import APIRouter

report_router = APIRouter()


@report_router.get(
    "",
    response_model=List[GetReportListResponseSchema],
    responses={"400": {"model": ExceptionResponseSchema}},
)
async def get_report_list():
    return await ReportService().get_report_list()
