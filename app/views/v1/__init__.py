from fastapi import APIRouter

from .configure import configure_router
from .report import report_router

sub_router = APIRouter()
sub_router.include_router(configure_router, prefix="/configure", tags=["Configure"])
sub_router.include_router(report_router, prefix="/report", tags=["Report"])


__all__ = ["configure_router", "report_router"]