import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    ENV: str = "development"
    DEBUG: bool = True
    APP_HOST: str = "0.0.0.0"
    APP_PORT: int = 8000


class DevelopmentConfig(Settings):
    db_url = os.getenv("DATABASE", "postgresql+psycopg2://postgres:admin@db/postgres")
    DB_URL: str = db_url


class ProductionConfig(Settings):
    db_url = os.getenv("DATABASE", "")
    DEBUG: str = False
    DB_URL: str = db_url


def get_settings():
    env = os.getenv("ENV", "development")
    config_type = {
        "development": DevelopmentConfig(),
        "production": ProductionConfig(),
    }
    return config_type[env]


settings: Settings = get_settings()
