DISCOUNT_PRICE = 2000.0

WheelRule = {
    'wheel1': ['battery1', 'battery2', 'battery3'],
    'wheel2': ['battery1', 'battery2', 'battery3'],
    'wheel3': ['battery2', 'battery3']
}

TireRule = {
    'tire1': ['wheel1', 'wheel2', 'wheel3'],
    'tire2': ['wheel2', 'wheel3'],
    'tire3': ['wheel3']
}