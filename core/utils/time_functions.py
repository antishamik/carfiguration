from datetime import datetime, timedelta

import pytz

TIMEZONE = pytz.timezone('Europe/Berlin')


class TimeFunctions:
    @staticmethod
    def current_date():
        return datetime.now(TIMEZONE).date()  # Today

    @staticmethod
    def get_the_last_day(target_dayofweek=4):  # Friday
        current_dayofweek = datetime.now(TIMEZONE).weekday()  # Today

        if target_dayofweek <= current_dayofweek:
            # target is in the current week
            end_date = datetime.now(TIMEZONE) - timedelta(current_dayofweek - target_dayofweek)

        else:
            # target is in the previous week
            end_date = datetime.now(TIMEZONE) - timedelta(weeks=1) + timedelta(target_dayofweek - current_dayofweek)

        return end_date.date()
